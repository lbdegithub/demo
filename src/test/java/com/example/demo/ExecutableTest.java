package com.example.demo;/**
 * @author LiBin
 * @version 1.0
 * @date Created in 2:31 下午 2020/4/10
 */

import com.example.demo.po.User;

import java.lang.reflect.*;
import java.util.ArrayList;

/**
 * @author LiBin
 * @email 22755@etransfa.com
 * @date 2020-04-10 2:31 下午
 **/
public class ExecutableTest {

    public static void main(String[] args) {

        Class<User> cls = User.class;

        ArrayList<String> methodsDesciption = getDeclaredMethodsList(cls);
        System.out.println("Declared Methods  for " + cls.getName());
        for (String desc : methodsDesciption) {
            System.out.println(desc);
        }
        methodsDesciption = getMethodsList(cls);
        System.out.println("\nMethods for  " + cls.getName());
        for (String desc : methodsDesciption) {
            System.out.println(desc);
        }

        System.out.println("\n\n\n------------------Constructor info---------------------");
        Constructor<?>[] constructors = cls.getConstructors();
        for (Constructor m : constructors) {
            System.out.println("------------------Constructor begin---------------------");
            System.out.println("方法名称    ：" + m.getName());
            System.out.println("方法修饰符 ： " + getModifiers(m));
            System.out.println(getParameters(m));
            System.out.println("异常信息：" + getExceptionList(m));
            System.out.println("------------------Constructor end---------------------");
        }

        System.out.println("\n\n\n------------------method info---------------------");

        for (Method m : cls.getMethods()) {
            System.out.println("------------------method begin---------------------");
            System.out.println("方法名称    ：" + m.getName());
            System.out.println("方法修饰符 ： " + getModifiers(m));
            System.out.println(getParameters(m));
            System.out.println("异常信息：" + getExceptionList(m));
            System.out.println("------------------method end---------------------");
        }
    }

    public static ArrayList<String> getMethodsList(Class c) {
        Method[] methods = c.getMethods();
        ArrayList<String> methodsList = getMethodsDesciption(methods);
        return methodsList;
    }

    public static ArrayList<String> getDeclaredMethodsList(Class c) {
        Method[] methods = c.getDeclaredMethods();
        ArrayList<String> methodsList = getMethodsDesciption(methods);
        return methodsList;
    }

    public static ArrayList<String> getMethodsDesciption(Method[] methods) {
        ArrayList<String> methodList = new ArrayList<>();

        for (Method m : methods) {
            String modifiers = getModifiers(m);

            Class returnType = m.getReturnType();
            String returnTypeName = returnType.getSimpleName();

            String methodName = m.getName();

            String params = getParameters(m).toString();

            String throwsClause = getExceptionList(m).toString();

            methodList.add(modifiers + "  " + returnTypeName + "  " + methodName
                    + "(" + params + ") " + throwsClause);
        }

        return methodList;
    }

    public static ArrayList<String> getParameters(Executable exec) {
        Parameter[] parms = exec.getParameters();
        ArrayList<String> parmList = new ArrayList<>();
        for (int i = 0; i < parms.length; i++) {

            int mod = parms[i].getModifiers() & Modifier.parameterModifiers();
            String modifiers = Modifier.toString(mod);
            String parmType = parms[i].getType().getSimpleName();
            String parmName = parms[i].getName();
            String temp = "参数修饰符：" + modifiers + " ---- 参数类型：" + parmType + " ---- 参数名：" + parmName;
            if (temp.trim().length() == 0) {
                continue;
            }
            parmList.add(temp.trim());
        }
        return parmList;
    }

    public static ArrayList<String> getExceptionList(Executable exec) {
        ArrayList<String> exceptionList = new ArrayList<>();
        for (Class<?> c : exec.getExceptionTypes()) {
            exceptionList.add(c.getSimpleName());
        }
        return exceptionList;
    }

    public static String getModifiers(Executable exec) {
        int mod = exec.getModifiers();
        if (exec instanceof Method) {
            mod = mod & Modifier.methodModifiers();
        } else if (exec instanceof Constructor) {
            mod = mod & Modifier.constructorModifiers();
        }
        return Modifier.toString(mod);
    }

}
