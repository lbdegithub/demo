package com.example.demo;

import com.example.demo.po.DITest;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class DemoApplicationTests {

    @Test
    void contextLoads() {
        DITest beanRegistry = new DITest();

        beanRegistry.rd("a", "b");
        beanRegistry.rd("a", "d");
        beanRegistry.rd("b", "c");
        beanRegistry.rd("c", "b");

        assertThat(beanRegistry.id("a", "b")).isTrue();
        assertThat(beanRegistry.id("b", "c")).isTrue();
        assertThat(beanRegistry.id("c", "b")).isTrue();
        assertThat(beanRegistry.id("a", "c")).isTrue();
        assertThat(beanRegistry.id("c", "a")).isFalse();
        assertThat(beanRegistry.id("b", "a")).isFalse();
        assertThat(beanRegistry.id("a", "a")).isFalse();
        assertThat(beanRegistry.id("b", "b")).isTrue();
        assertThat(beanRegistry.id("c", "c")).isTrue();
    }

}
