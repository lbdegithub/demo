package com.example.demo;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.util.Scanner;

/**
 * @author LiBin
 * @since 2020-05-20 4:00 下午
 **/
public class MappedByteBufferTest {

    public static void main(String[] args) throws Exception {
        File file = new File("/Users/lb/Downloads/idea快捷键.txt");
        long len = file.length();
        byte[] ds = new byte[(int) len];

        MappedByteBuffer mappedByteBuffer = new FileInputStream(file)
                .getChannel().map(FileChannel.MapMode.READ_ONLY, 0, len);

        for (int offset = 0; offset < len; offset++) {
            byte b = mappedByteBuffer.get();
            ds[offset] = b;
        }

        Scanner scan = new Scanner(new ByteArrayInputStream(ds)).useDelimiter(" ");
        while (scan.hasNext()) {
            System.out.print(scan.next() + " ");
        }

        //String files[]=new String[1];
        //files[0]="/Users/lb/Downloads/idea快捷键.txt";
        //catFiles(Channels.newChannel(System.out), files);
    }

    private static void catFiles(WritableByteChannel target, String[] files)
            throws Exception {
        for (int i = 0; i < files.length; i++) {
            FileInputStream fis = new FileInputStream(files[i]);
            FileChannel channel = fis.getChannel();
            channel.transferTo(0, channel.size(), target);
            channel.close();
            fis.close();
        }
    }
}
