package com.example.demo;/**
 * @author LiBin
 * @version 1.0
 * @date Created in 6:07 下午 2020/4/7
 */

/**
 * @author LiBin
 * @email 22755@etransfa.com
 * @date 2020-04-07 6:07 下午
 **/
public class Test {
    public static void main(String[] args) {
        String a = "123";
        String b = "1" + "2";
        String c = a + "llll";
        String d = new String("111") + new String("222");
    }
}
