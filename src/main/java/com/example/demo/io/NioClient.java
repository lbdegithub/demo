package com.example.demo.io;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.concurrent.TimeUnit;

/**
 * @author LiBin
 * @since 2020-05-25 5:13 下午
 **/
public class NioClient {

    public static void main(String[] args) {
        SocketChannel socketChannel = null;
        try {
            ByteBuffer buffer = ByteBuffer.allocate(1024);

            socketChannel = SocketChannel.open();
            socketChannel.configureBlocking(false);
            socketChannel.connect(new InetSocketAddress(8081));

            if (socketChannel.finishConnect()) {
                String headMsg = "Register coming in";
                buffer.put(headMsg.getBytes());
                buffer.flip();
                while (buffer.hasRemaining()) {
                    socketChannel.write(buffer);
                }
                if (true) {
                    //return;
                }

                System.out.println("1111112323232323");
                int i = 0;
                while (true) {

                    TimeUnit.SECONDS.sleep(1);
                    if ((socketChannel.validOps() & SelectionKey.OP_READ) == SelectionKey.OP_READ) {
                        int len = 0;
                        buffer.clear();
                        while ((len = socketChannel.read(buffer)) > 0) {
                            String msg = new String(buffer.array(), 0, len);
                            System.out.println(Thread.currentThread().getName() + "-->\n" + msg);
                        }
                    }

                    if ((socketChannel.validOps() & SelectionKey.OP_WRITE) == SelectionKey.OP_WRITE) {
                        String info = "我是客户端，第" + i + "次发送";
                        buffer.clear();
                        buffer.put(info.getBytes());
                        buffer.flip();
                        while (buffer.hasRemaining()) {
                            System.out.println(buffer);
                            socketChannel.write(buffer);
                        }
                    }

                }
            }

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                if (socketChannel != null) {
                    socketChannel.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
