package com.example.demo.io;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.*;

/**
 * @author LiBin
 * @since 2020-05-25 5:13 下午
 **/
public class NioServer {

    private static Vector<SelectionKey> chaters = new Vector<>(8);

    public static void main(String[] args) {

        ServerSocketChannel serverSocketChannel = null;
        Selector selector = null;

        try {
            serverSocketChannel = ServerSocketChannel.open();

            serverSocketChannel.bind(new InetSocketAddress(8081));

            serverSocketChannel.configureBlocking(false);

            selector = Selector.open();

            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

            while (true) {
                if (Thread.currentThread().isInterrupted()) {
                    return;
                }

                int readyOperation = selector.select();
                System.out.println("准备好的客户数" + readyOperation);

                Set<SelectionKey> selectionKeys = selector.selectedKeys();
                Iterator<SelectionKey> iterator = selectionKeys.iterator();
                while (iterator.hasNext()) {
                    SelectionKey selectionKey = iterator.next();
                    iterator.remove();
                    process(selectionKey);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != selector) {
                    selector.close();
                }
                if (null != serverSocketChannel) {
                    serverSocketChannel.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private static void process(SelectionKey selectionKey) throws IOException {
        if (selectionKey.isAcceptable()) {
            doAcceptEvent(selectionKey);
        } else if (selectionKey.isReadable()) {
            doReadEvent(selectionKey);
        } else if (selectionKey.isWritable()) {
            doWriteEvent(selectionKey);
        }
    }

    private static void doWriteEvent(SelectionKey selectionKey) throws IOException {
        System.out.println("准备向客户端写数据");
        List<String> list = (List<String>) selectionKey.attachment();
        SocketChannel sc = (SocketChannel) selectionKey.channel();
        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
        if (list == null) {
            list = new ArrayList<>();
            list.add("默认消息");
        }
        Iterator<String> iterReader = list.iterator();
        while (iterReader.hasNext()) {
            String message = iterReader.next();
            byteBuffer.clear();
            byteBuffer.put(message.getBytes());
            byteBuffer.flip();
            while (byteBuffer.hasRemaining()) {
                sc.write(byteBuffer);
            }
            System.out.println("Writing message is " + message);
            iterReader.remove();
        }
        selectionKey.interestOps(SelectionKey.OP_READ);
    }

    private static void doReadEvent(SelectionKey selectionKey) throws IOException {
        System.out.println("准备读取客户端的数据");
        SocketChannel client = (SocketChannel) selectionKey.channel();
        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
        //ByteBuffer byteBuffer = (ByteBuffer) selectionKey.attachment();
        int read = client.read(byteBuffer);
        StringBuilder builder = new StringBuilder();
        if (read > 0) {
            byteBuffer.flip();
            // while (byteBuffer.hasRemaining()) {
            builder.append(new String(byteBuffer.array()), 0, read);
            //}
            System.out.println("收到：----->" + builder.toString());
            byteBuffer.clear();
            read = client.read(byteBuffer);
        }

        Iterator<SelectionKey> iterWriter = chaters.iterator();
        while (iterWriter.hasNext()) {
            SelectionKey keyWriter = iterWriter.next();
            List<String> list = (List<String>) keyWriter.attachment();
            if (list == null) {
                list = new ArrayList<>();
                keyWriter.attach(list);
            }
            list.add(builder.toString());
        }
        //selectionKey.interestOps(selectionKey.interestOps() | SelectionKey.OP_WRITE);
        client.register(selectionKey.selector(), SelectionKey.OP_WRITE);

        if (read == -1) {
            client.close();
        }
    }

    private static void doAcceptEvent(SelectionKey selectionKey) throws IOException {
        System.out.println("准备接收客户端的连接");
        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) selectionKey.channel();
        SocketChannel client = serverSocketChannel.accept();
        client.configureBlocking(false);
        //client.register(selectionKey.selector(), SelectionKey.OP_READ, ByteBuffer.allocateDirect(1024));
        chaters.add(client.register(selectionKey.selector(), SelectionKey.OP_READ));
    }
}
