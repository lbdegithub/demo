package com.example.demo.lock;/**
 * @author LiBin
 * @version 1.0
 * @date Created in 8:29 上午 2020/6/8
 */

import java.util.concurrent.*;

/**
 * @author LiBin
 * @email 22755@etransfa.com
 * @since 2020-06-08 8:29 上午
 **/
public class ThreadPoolExecutorDemo extends Thread {

    public static void main(String[] args) {

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(3, 3, 0,
                TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        for (int i = 0; i < 10; i++) {
            executorService.execute(new ThreadPoolExecutorDemo());
        }
        //executorService.
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + "--执行。。。");
    }
}
