package com.example.demo.lock;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * @author LiBin
 * @since 2020-06-01 5:18 下午
 **/
public class SemaphoreDemo extends Thread {

    private final Semaphore semaphore;

    public SemaphoreDemo(Semaphore semaphore) {
        this.semaphore = semaphore;
    }

    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(3);
        for (int i = 0; i < 10; i++) {
            new SemaphoreDemo(semaphore).start();
        }
    }

    @Override
    public void run() {
        try {
            semaphore.acquire();
            System.out.println(Thread.currentThread().getName() + "-获取到资源。");
            TimeUnit.SECONDS.sleep(3);
            System.out.println(Thread.currentThread().getName() + "-执行完毕。");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println(Thread.currentThread().getName() + "-释放资源。");
            semaphore.release();
        }
    }
}
