package com.example.demo.lock;

/**
 * @author LiBin
 * @since 2020-05-25 5:13 下午
 **/
public class SynchronnizedDome {

    private static int ii = 0;

    private static synchronized void inc() {
        ii++;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 1000; i++) {
            new Thread(SynchronnizedDome::inc).start();
        }
        System.out.println(ii);
        //SocketChannel
    }
}
