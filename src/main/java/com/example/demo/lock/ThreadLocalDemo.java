package com.example.demo.lock;/**
 * @author LiBin
 * @version 1.0
 * @date Created in 9:44 上午 2020/6/16
 */

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author LiBin
 * @email 22755@etransfa.com
 * @since 2020-06-16 9:44 上午
 **/
public class ThreadLocalDemo {

    public AtomicInteger atomicInteger = new AtomicInteger();

    public static void main(String[] args) {
        ThreadLocal<Integer> d = new ThreadLocal<>();
        ThreadLocalDemo demo = new ThreadLocalDemo();
        for (int i = 0; i < 50; i++) {
            int a = demo.atomicInteger.getAndAdd(0x61c88647);
            System.out.println(a & 15);
        }
    }
}
