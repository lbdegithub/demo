package com.example.demo.lock;

import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author LiBin
 * @since 2020-06-02 2:37 下午
 **/
public class ConcurrentHashMapDemo {
    public static void main(String[] args) {

        //System.out.println(Integer.numberOfLeadingZeros(16) | (1 << (16 - 1)));
        //scene1();
        scene2();

        //System.out.println( Runtime.getRuntime().availableProcessors());
    }

    private static void scene1() {
        ConcurrentHashMap<String, String> concurrentHashMap = new ConcurrentHashMap<>();
        System.out.println(concurrentHashMap.size());
        for (int i = 0; i < 27; i++) {
            int finalI = i;
            //new Thread(() -> concurrentHashMap.put("1", finalI + "")).start();
            concurrentHashMap.put("1"+i, finalI + "");
        }
        System.out.println(concurrentHashMap.size());
        concurrentHashMap.put("1", "1");
        System.out.println(concurrentHashMap.size());

        int a = 123;
        System.out.println(Integer.hashCode(a));
        String b = "a";
        System.out.println(b.hashCode());
    }

    private static void scene2() {
        ConcurrentHashMap<HashDemo, String> concurrentHashMap = new ConcurrentHashMap<>();
        System.out.println(concurrentHashMap.size());
        for(int i =0; i<16;i++) {
            concurrentHashMap.put(new HashDemo(1, "a"+i), "1a"+i);
            System.out.println(concurrentHashMap.size());
        }
        concurrentHashMap.put(new HashDemo(1, "b"), "1b");
        System.out.println(concurrentHashMap.size());
    }

    static class HashDemo {
        private Integer id;

        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public HashDemo(Integer id, String name) {
            this.id = id;
            this.name = name;
        }

        public HashDemo() {
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            HashDemo hashDemo = (HashDemo) o;
            return Objects.equals(id, hashDemo.id) &&
                    Objects.equals(name, hashDemo.name);
        }

        @Override
        public int hashCode() {
            return id;
        }
    }
}
