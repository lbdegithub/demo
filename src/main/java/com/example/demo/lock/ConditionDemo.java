/*
 * @author LiBin
 * @version 1.0
 * @date Created in 1:44 下午 2020/6/1
 */
package com.example.demo.lock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author LiBin
 * @since 2020-06-01 1:44 下午
 **/
public class ConditionDemo extends Thread {

    private final ReentrantLock reentrantLock;

    private final Condition condition;

    public ConditionDemo(ReentrantLock reentrantLock, Condition condition) {
        this.reentrantLock = reentrantLock;
        this.condition = condition;
    }


    public static void main(String[] args) throws InterruptedException {
        ReentrantLock reentrantLock = new ReentrantLock();
        Condition condition = reentrantLock.newCondition();
        Thread t1 = new ConditionDemo(reentrantLock, condition);
        ConditionDemo t2 = new ConditionDemo(reentrantLock, condition);
        t1.start();
        TimeUnit.SECONDS.sleep(1);
        System.out.println("唤醒t1");
        t2.signal();
    }

    @Override
    public void run() {
        reentrantLock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + " 开始执行。。。。");
            condition.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            reentrantLock.unlock();
            System.out.println(Thread.currentThread().getName() + " 结束执行。。。。");
        }
    }

    public void signal() {
        reentrantLock.lock();
        try {
            condition.signal();
        } finally {
            reentrantLock.unlock();
        }
    }
}
