package com.example.demo.lock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @author LiBin
 * @email 22755@etransfa.com
 * @since 2020-05-29 4:54 下午
 **/
public class ReentrantLockDemo {

    private static int count;

    private static ReentrantLock reentrantLock = new ReentrantLock();

    private static void inc() {
        reentrantLock.lock();
        try {
            count++;
        } finally {
            reentrantLock.unlock();
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 1000; i++) {
            new Thread(ReentrantLockDemo::inc, "thread" + i).start();
        }
        System.out.println(count);
    }
}
