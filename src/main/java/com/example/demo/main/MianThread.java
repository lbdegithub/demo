package com.example.demo.main;
/**
 *
 * @author LiBin
 * @since 2020-06-23 5:27 下午
 **/
public class MianThread {

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(()-> System.out.println("t1"));
        t1.start();
        t1.join();
        Thread t2 = new Thread(()-> System.out.println("t2"));
        t2.start();
        t2.join();
        Thread t3 = new Thread(()-> System.out.println("t3"));
        t3.start();
        t3.join();
        System.out.println("main");
    }
}
