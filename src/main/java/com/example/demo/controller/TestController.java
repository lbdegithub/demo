package com.example.demo.controller;

import com.example.demo.service.OrderService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author LiBin
 * @date 2020-03-13 1:45 下午
 **/
@RestController
@RequestMapping("/user")
public class TestController {
    //public TestController() {
    //    System.out.println("-------------bean created-----------");
    //}

    private String  a = "A";

    //@Lazy
    //@Resource
    //private UserService userService;

    @Autowired
    private OrderService orderService;

    @GetMapping("/get")
    public String getUser() {
        //userService.load();

        orderService.load();

        return "user1";
    }
}
