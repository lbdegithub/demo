package com.example.demo.service;/**
 * @author LiBin
 * @version 1.0
 * @date Created in 8:00 下午 2020/4/12
 */

/**
 * @author LiBin
 * @email 22755@etransfa.com
 * @date 2020-04-12 8:00 下午
 **/
public interface OrderService {
    Object load();
}
