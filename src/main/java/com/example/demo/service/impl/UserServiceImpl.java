package com.example.demo.service.impl;/**
 * @author LiBin
 * @version 1.0
 * @date Created in 8:02 下午 2020/4/12
 */

import com.example.demo.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @author LiBin
 * @email 22755@etransfa.com
 * @date 2020-04-12 8:02 下午
 **/
//@Service
public class UserServiceImpl implements UserService {

    public UserServiceImpl() {
        System.out.println("UserService");
    }

    @Override
    public Object load() {
        return null;
    }

    public static void main(String[] args) {
        //System.out.println("1234567890abcdf".substring(0,14));

        Dome dome = new Dome();
        System.out.println(Thread.currentThread().getName());
        //dome.run();

        Thread a = new Thread(dome);
        a.start();

        Dome1 b = new Dome1();
        b.start();
    }

    static class Dome implements Runnable{

        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName());
            System.out.println("dome come in");
        }
    }

    static class Dome1 extends Thread{
        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName());
            System.out.println("dome1 come in");
        }
    }
}
