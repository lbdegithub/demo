package com.example.demo.service.impl;/**
 * @author LiBin
 * @version 1.0
 * @date Created in 8:02 下午 2020/4/12
 */

import com.example.demo.controller.TestController;
import com.example.demo.service.OrderService;
import org.openjdk.jol.info.ClassLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author LiBin
 * @email 22755@etransfa.com
 * @date 2020-04-12 8:02 下午
 **/
@Service
public class OrderServiceImpl implements OrderService {
    public OrderServiceImpl() {
        System.out.println("OrderService");
    }

    @Autowired
    private TestController testController;

    @Override
    public Object load() {
        return null;
    }

    public static void main(String[] args) {
        OrderServiceImpl orderService =new OrderServiceImpl();
        System.out.println(ClassLayout.parseInstance(orderService).toPrintable());
        synchronized (orderService){
            //orderService.hashCode(); //升级到重量级锁
            System.out.println(ClassLayout.parseInstance(orderService).toPrintable());
        }
    }
}
