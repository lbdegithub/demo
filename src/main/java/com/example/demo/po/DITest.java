package com.example.demo.po;/**
 * @author LiBin
 * @version 1.0
 * @date Created in 3:34 下午 2020/4/9
 */

import org.springframework.beans.factory.support.DefaultSingletonBeanRegistry;

/**
 * @author LiBin
 * @email 22755@etransfa.com
 * @date 2020-04-09 3:34 下午
 **/
public class DITest extends DefaultSingletonBeanRegistry {

    public void rd(String name, String dName) {
        registerDependentBean(name, dName);
    }

    public boolean id(String name, String dName) {
        return isDependent(name, dName);
    }
}
