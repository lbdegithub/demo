package com.example.demo.po;/**
 * @author LiBin
 * @version 1.0
 * @date Created in 3:32 下午 2020/4/9
 */

import lombok.Data;

import java.lang.reflect.Method;

/**
 * @author LiBin
 * @email 22755@etransfa.com
 * @date 2020-04-09 3:32 下午
 **/
@Data
public class User {

    private Integer id;

    private String name;

    public User(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public User() {

    }

    protected void test() {
        System.out.println("test");
    }

    public String toString123() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public String toString123(final String a, int b) {
        return "User{" +
                "id=" + a +
                ", name='" + b + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Class clazz = User.class;
        Method[] methods = clazz.getMethods();
        for (Method method : methods) {
            System.out.println(method.getName()+"------"+ method.getGenericParameterTypes());
            System.out.println(method.getName()+"------"+ method.getParameterTypes());
        }

    }
}
