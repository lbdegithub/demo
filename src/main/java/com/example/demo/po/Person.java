package com.example.demo.po;

public interface Person<T> {

    T getName(T param);
}
